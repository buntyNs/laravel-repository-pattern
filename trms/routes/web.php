<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('login');
})->middleware('auth');


Route::get('/userView', function () {
    return view('admin.view_user');
});


//login route
Route::get('/login', 'LoginController@create')->name('login');
Route::post('/login', 'LoginController@login');
Route::post('/logout', 'LoginController@logout');


Route::middleware(['auth','agent'])->group(function () {
    Route::get('/agent/dashboard', 'UserController@create');
});


Route::middleware(['auth','admin'])->group(function () {
    Route::get('/create_user', 'UserController@create');
    Route::get('/admin/dashboard', 'UserController@create');
});

