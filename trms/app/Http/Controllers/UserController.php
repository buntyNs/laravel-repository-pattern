<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Session;
use Redirect;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        
    }

    public function index()
    {
        //
    }

   
    public function create()
    {
        return view('admin.create_user');
    }

   
    public function store(Request $request)
    {
            
    $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'plant_array'=>'required',
            'contact'=>'required',
            'role'=>'required'
        ])->validate();

      
        User::create([
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'email' => request('email'),
            'contact' => request('contact'),
            'role' => request('role'),
            'plant' => request('plant_array'),
            'password' => Hash::make(request('password')),
        ]);
        Session::flash('message1', 'My message');
        return Redirect::to('/create_user');

    
        
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
