<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{



    public function create(){
        return view('login');
    }

    public function login(Request $request){
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials) && Auth::user()->role == 'admin') {
            // Authentication passed
            return redirect('/admin/dashboard');
        }elseif(Auth::attempt($credentials) && Auth::user()->role == 'agent'){
            return redirect('/agent/dashboard');
        }

        return back()->withErrors([
            'message' => 'Please check your credentials and try again'
        ]);

    }

    

    public function logout(){
        

    }


}
