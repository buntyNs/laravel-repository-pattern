@extends('master')

@section('css')
<link href="css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">
@endsection
@section('content')
    @include('layouts.page_title',['title' => 'Create New User', 'subTitle' => 'create_user','action'=>'View Users','actionPath'=>""])
    <div class="wrapper wrapper-content animated fadeInRight">
    @if (Session::has('message1'))
    <div class="alert alert-info">{{ Session::get('message1') }}</div>
@endif
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <form method="POST"  action = "/create_user" id = "user_data">
                            @csrf
                                <div class="form-group  row"><label class="col-sm-2 col-form-label">First Name</label>
                                    <div class="col-sm-10">
                                        <input name = "first_name" type="text" class="form-control" value = "{{old('first_name')}}" >
                                        @if ($errors->has('first_name'))
                                        <div class="error">{{ $errors->first('first_name') }}</div>
                                        @endif
                                </div>
                                </div>

                                <div class="form-group  row"><label class="col-sm-2 col-form-label">Last Name</label>
                                    <div class="col-sm-10"><input name = "last_name" type="text" class="form-control" value = "{{old('last_name')}}">
                                    @if ($errors->has('last_name'))
                                        <div class="error">{{ $errors->first('last_name') }}</div>
                                        @endif
                                </div>
                                </div>

                                <div class="form-group  row"><label class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10"><input name= "email" type="email" class="form-control" value = "{{old('email')}}" >
                                    @if ($errors->has('email'))
                                        <div class="error">{{ $errors->first('email') }}</div>
                                        @endif
                                </div>
                                </div>

                                <div class="form-group  row"><label class="col-sm-2 col-form-label">Contact No</label>
                                    <div class="col-sm-10"><input name = "contact" type="text" class="form-control" value = "{{old('contact')}}">
                                    @if ($errors->has('contact'))
                                        <div class="error">{{ $errors->first('contact') }}</div>
                                        @endif
                                </div>
                                </div>

                                <div class="form-group row"><label class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10"><input type="password" class="form-control" name="password" value = "{{old('password')}}">
                                    @if ($errors->has('password'))
                                        <div class="error">{{ $errors->first('password') }}</div>
                                        @endif
                                </div>
                                </div>
                                <div class="hr-line-dashed">
                                </div>
                                <div class="form-group row"><label class="col-sm-2 col-form-label">Plants</label>
                                    <div class="col-sm-10">
                                    @if ($errors->has('plant_array'))
                                        <div class="error">Please select at least one plant for user</div>
                                        @endif
                                    <div id="form" action = "create_user" class="wizard-big">
                                        <select id = "box2View" name = "plant" class="form-control dual_select" multiple>
                                            <option value="United States">United States</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Austria">Austria</option>
                                            <option value="Bahamas">Bahamas</option>
                                            <option value="Barbados">Barbados</option>
                                            <option value="Belgium">Belgium</option>
                                            <option value="Bermuda">Bermuda</option>
                                            <option value="Brazil">Brazil</option>
                                            <option value="Bulgaria">Bulgaria</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Canada">Canada</option>
                                        </select>
                                    </div>
                                    
                                    </div>
                                </div>
                                <input type="hidden" name="plant_array" value="null" id="setState">
                                <input type="hidden" name="role" value="agent" id="setState">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-white btn-sm" type="submit">Cancel</button>
                                        <button class="btn btn-primary btn-sm" type="submit">Save changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('script')
   <!-- Dual Listbox -->
   <script src="js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
<script>
$('.dual_select').bootstrapDualListbox({
                selectorMinimalHeight: 160
            });
            $('#user_data').submit(function(event) {
                var items = $("select#bootstrap-duallistbox-selected-list_plant option");
                var plant_array = [];
                var i;
            for (i = 0; i < items.length; i++) { 
            plant_array.push(items[i].value);
            }
            $('#setState').val(plant_array);

            });
</script>
@endsection